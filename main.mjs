import { Table } from "./tabel.mjs";
  
  const table = new Table({
    columns: ["Name", "Email"],
    data: [ 
      ["Muhammad Hudzaifah", "m.hudzaifah.mh@gmail.com"],
      ["Audi Fikri", "Audi@gmail.com"]
    ]
  });
  const app = document.getElementById("app");
  table.render(app);